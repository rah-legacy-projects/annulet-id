var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-auth-models')
    .modeler,
    crypto = require('crypto'),
    moment = require('moment'),
    util = require('util');

require('annulet-config');
module.exports = exports = {
    getUser: function(req, res, next) {
        var query = {
        };

        if(/^[0-9a-fA-F]{24}$/.test(req.params.user)){
            query._id = req.params.user;
        }else{
            query.email = req.params.user;
        }
        async.waterfall([

            function(cb) {
                modeler.db({
                    collection: 'User',
                    query: query
                }, cb);
            },
            function(models, cb) {
                if (!models) {
                    cb('auth token could not identify modelset!');
                } else {
                    cb(null, models);
                }
            },
            function(models, cb) {
                models.User.findOne(query)
                    .exec(function(err, user) {
                        if (!!err) {
                            logger.error('problem getting user: ' + util.inspect(err));
                        }
                        cb(err, user);
                    });
            }
        ], function(err, user) {
            user = user || {};
            res.err = err;
            res.data = {
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                _id: user._id
            };
            next();
        });
    },
    saveUser: function(req, res, next) {
        logger.warn('saving user');
        var query = {};
        if (req.params.user == 'new') {
            query.email = req.body.user.email
        } else {
            query._id = req.params.user.toObjectId();
        }
        async.waterfall([

            function(cb) {
                logger.silly('[save user] getting models');

                modeler.db({
                    collection: 'User',
                    query: query
                }, cb);
            },
            function(models, cb) {
                if (!models) {
                    cb('auth token could not identify modelset!');
                } else {
                    logger.silly('[save user] keys: ' + util.inspect(_.keys(models)));
                    cb(null, models);
                }
            },
            function(models, cb) {
                logger.silly('[save user] getting user');
                models.User.findOne(query)
                    .exec(function(err, user) {
                        if (!!err) {
                            logger.error('problem getting user: ' + util.inspect(err));
                        }
                        cb(err, models, {
                            user: user
                        });
                    });
            },
            function(models, p, cb) {
                logger.silly('[save user] managing user');
                var isNew = false;
                if (!p.user) {
                    logger.silly('[save user] user is new');
                    p.user = new models.User();
                    isNew = true;
                }
                p.user.active = true;
                p.user.deleted = false;
                if (!/^\s*$/.test((req.body.user.password || ''))) {
                    p.user.password = req.body.user.password;
                }
                p.user.firstName = req.body.user.firstName;
                p.user.lastName = req.body.user.lastName;
                p.user.email = req.body.user.email;

                p.user.save(function(err, user) {
                    if (!!err) {
                        logger.error('[save user] problem saving user: ' + util.inspect(err));
                    }
                    p.user = user.toObject();
                    p.user.isNew = isNew;
                    cb(err, models, p);
                });

            }
        ], function(err, models, p) {
            logger.silly('[save user] finished');
            res.err = err;
            res.data = p.user;
            next();
        });
    }
};
