var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-auth-models')
    .modeler,
    crypto = require('crypto'),
    moment = require('moment'),
    configuration = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill,
    util = require('util');

require('annulet-config');
module.exports = exports = {
    validateSlug: function(req, res, next) {
        modeler.db({}, function(err, models) {
            models.ForgotPassword.findOne({
                slug: req.params.slug,
                used: false,
                $or: [{
                    expirationDate: null
                }, {
                    expirationDate: {
                        $gte: new Date()
                    }
                }]
            })
                .exec(function(err, forgotPassword) {
                    if (!forgotPassword) {
                        res.err = 'Password reset not found.';
                    } else {
                        res.err = err;
                        res.data = forgotPassword;
                    }
                    next();
                });
        });
    },
    cancel: function(req, res, next) {
        async.waterfall([

            function(cb) {
                //get models
                modeler.db({}, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                //identify user
                models.ForgotPassword.findOne({
                    slug: req.params.slug
                })
                    .exec(function(err, forgotPassword) {
                        forgotPassword.used = true;
                        forgotPassword.save(function(err) {
                            res.err = err;
                            cb(err, models, p);
                        });
                    });
            },
        ], function(err, models, p) {
            res.err = err;
            next();
        });
    },
    changePassword: function(req, res, next) {
        async.waterfall([

            function(cb) {
                //get models
                modeler.db({}, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                logger.silly('[change pw] identifying slug');
                //identify user
                models.ForgotPassword.findOne({
                    slug: req.params.slug
                })
                    .exec(function(err, forgotPassword) {
                        p.forgotPassword = forgotPassword;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                logger.silly('[change pw] identifying user');
                models.User.findOne({
                    _id: p.forgotPassword.user
                })
                    .exec(function(err, user) {
                        p.user = user;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                logger.silly('[change pw] spending slug');
                //spend the slug
                p.forgotPassword.used = true;
                p.forgotPassword.save(function(err) {
                    if(!!err){
                        logger.error('[change pw] error spending slug: ' + util.inspect(err));
                    }
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[change pw] updating pword');
                //save the updated password
                p.user.password = req.body.password;
                p.user.save(function(err) {
                    if(!!err){
                        logger.error('[change pw] error updating pword: ' + util.inspect(err));
                    }
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            next();
        });
    },
    forgot: function(req, res, next) {
        //add slugged record
        //queue email
        async.waterfall([

            function(cb) {
                //get models
                modeler.db({}, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                logger.silly('[forgot] looking for email: ' + req.body.email);
                //identify user
                models.User.findOne({
                    email: req.body.email
                })
                    .exec(function(err, user) {
                        if (!user) {
                            cb({problem: 'Email address not found.'});
                        } else {
                            p.user = user;
                            cb(err, models, p);
                        }
                    });
            },
            function(models, p, cb) {
                logger.silly('[forgot] adding reset slug');
                //add forgot password
                var slug = (function(len) {
                    var buf = [];
                    chars = 'ABCDEGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                    charlen = chars.length;
                    for (var i = 0; i < len; ++i) {
                        buf.push(chars[Math.floor(Math.random() * (charlen + 1))])
                    }
                    return buf.join('');
                })(32);
                (new models.ForgotPassword({
                    user: p.user._id,
                    slug: slug,
                    expirationDate: moment()
                        .add(3, 'days')
                        .toDate(),
                    used: false
                }))
                    .save(function(err, forgot) {
                        if(!!err){
                            logger.error('[forgot] ' + util.inspect(err));
                        }
                        p.forgot = forgot;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                logger.silly('[forgot] enqueing email via mandrill');
                //queue up an email
                var mandrillClient = new Mandrill(configuration.configuration.mandrill.privateKey());

                mandrillClient.messages.sendTemplate({
                    template_name: 'Reset Password',
                    template_content: {},
                    message: {
                        "to": [{
                            "email": p.user.email,
                            "name": p.user.firstName + ' ' + p.user.lastName,
                            "type": "to"
                        }],
                        "merge_vars": [{
                            "rcpt": p.user.email,
                            "vars": [{
                                name: "PW_RESET",
                                content: configuration.configuration.paths.uiUri() + "/forgot/reset/" + p.forgot.slug
                            }, {
                                name: "CANCEL_PW_RESET",
                                content: configuration.configuration.paths.uiUri() + "/forgot/cancel/" + p.forgot.slug
                            }]
                        }],
                    },
                    async: false,
                    ip_pool: 'Main Pool'
                }, function(result) {
                    //email sent.
                    logger.silly('[forgot] email result: ' + util.inspect(result));
                    cb(null);
                }, function(error) {
                    //email not sent...?
                    logger.error('[forgot] error sending email via mandrill: ' + util.inspect(error));
                    cb(error);
                });
            }
        ], function(err, p) {
            if(!!err && !!err.problem){
                res.data = err;
            }else{
                res.err = err;
            }
            next();
        });
    }
};
