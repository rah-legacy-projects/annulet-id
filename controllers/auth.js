var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-auth-models')
    .modeler,
    crypto = require('crypto'),
    request = require('request'),
    moment = require('moment'),
    os = require('os'),
    paths = require('annulet-config')
    .configuration.paths,
    util = require('util');

module.exports = exports = {
    login: function(req, res) {

        var makeException = function(err, cb) {
            var who = (/^\s*$/.test(req.body.email || '')) ? '(no email provided)' : req.body.email;
            logger.silly(req.body.email + " is " + who);
            request.post({
                url: paths.apiUri() + '/problem',
                headers: {},
                body: {
                    environment: process.env.ENV,
                    server: {
                        hostname: os.hostname(),
                        type: os.type(),
                        platform: os.platform(),
                        arch: os.arch(),
                        release: os.release(),
                        uptime: os.uptime(),
                        loadavg: os.loadavg(),
                        freemem: os.freemem(),
                        totalmem: os.totalmem(),
                    },
                    exception: err,
                    customer: "[login, no customer available]",
                    userAgent: req.useragent,
                    createdBy: who,
                    modifiedBy: who
                },
                json: true
            }, function(err, response, body) {
                if (!!err) {
                    logger.error("!!! ERROR IN EXCEPTION TRACKING for login: " + util.inspect(err));
                }
                cb(err, body.ticketNumber);
            });
        };

        logger.silly('email to find: ' + req.body.email);
        async.waterfall([

            function(cb) {
                modeler.db({
                    collection: 'auth.User',
                    query: {
                        email: req.body.email
                    }
                }, function(err, models) {
                    if (!models) {
                        makeException('Critical error retrieving user, no modelsets able to be identified', function(err, ticketNumber) {
                            return cb('Critical error retrieving user.');
                        })
                    }

                    cb(err, models);
                });
            },
            function(models, cb) {
                logger.silly('login called for ' + req.body.email);
                //find user
                models.User.findOne({
                    email: req.body.email,
                    password: req.body.password
                })
                    .exec(function(err, user) {
                        cb(err, models, user);
                    });
            }
        ], function(err, models, user) {
            if (!!err) {
                makeException("problem retrieving user: " + util.inspect(err), function(err, ticketNumber) {
                    logger.error("db error retrieving user: " + util.inspect(err));
                    res.status(500)
                        .json({
                            err: {
                                message: 'Internal error.  Please contact your Annulet administrators.',
                                fatal: false
                            }
                        });
                });
            } else if (!user) {
                makeException("User not found", function(err, ticketNumber) {
                    logger.warn("User not found: " + req.body.email);
                    res.status(403)
                        .json({
                            err: {
                                message: "User not found.",
                                fatal: false
                            }
                        });
                });
            } else if (!user.active) {
                makeException("User inactive", function(err, ticketNumber) {
                    logger.warn("User not active: " + req.body.email);
                    res.status(403)
                        .json({
                            err: {
                                message: "User is not active.",
                                fatal: false
                            }
                        });
                });
            } else {
                //make a token
                var token = (function(len) {
                    var buf = [];
                    chars = 'ABCDEGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                    charlen = chars.length;
                    for (var i = 0; i < len; ++i) {
                        buf.push(chars[Math.floor(Math.random() * (charlen + 1))])
                    }
                    return buf.join('');
                })(256);
                var tokenHash = crypto.createHash('sha1')
                    .update(token)
                    .digest('hex');
                var expirationDate = moment()
                    .add(1, 'hours')
                    .toDate();

                logger.silly('token hash: ' + tokenHash);

                new models.AccessToken({
                    token: tokenHash,
                    expirationDate: expirationDate,
                    user: user._id,
                    isActive: true
                })
                    .save(function(err, token) {
                        if (!!err) {
                            makeException("error making token: " + util.inspect(err), function(merr, ticketNumber) {
                                logger.err(util.inspect(err));
                                res.status(500)
                                    .json({
                                        message: err.message,
                                        fatal: true
                                    });
                            });
                        } else {
                            logger.silly('login successful, returning token.');
                            res.status(200)
                                .json({
                                    err: err,
                                    data: token.token
                                })
                        }

                    });
            }
        });
    },
    logout: function(req, res) {
        logger.silly('token: ' + req.token);
        async.waterfall([

            function(cb) {
                modeler.db({
                    collection: 'auth.AccessToken',
                    query: {
                        token: req.token
                    }
                }, cb);
            },
            function(models, cb) {
                logger.silly('models retrieved');
                models.AccessToken.findOne({
                    token: req.token
                })
                    .exec(function(err, token) {
                        cb(err, token);
                    });
            }
        ], function(err, token) {
            token.isActive = false;
            token.save(function(err) {
                res.json({
                    err: err
                });
            });
        });
    },
    getUserInfo: function(req, res) {
        var authToken = req.get('annulet-auth-token') || req.query['access_token'];
        logger.silly('[get user info] token: ' + authToken);
        async.waterfall([

            function(cb) {
                modeler.db({
                    collection: 'auth.AccessToken',
                    query: {
                        token: authToken
                    }
                }, cb);
            },
            function(models, cb) {
                if (!models) {
                    cb('auth token could not identify modelset!');
                } else {
                    cb(null, models);
                }
            },
            function(models, cb) {
                models.AccessToken.findOne({
                    token: authToken,
                    isActive: true
                })
                    .populate({
                        path: 'user',
                        model: models.User
                    })
                    .exec(function(err, token) {
                        if (!!err) {
                            logger.error('problem getting token: ' + util.inspect(err));
                        }
                        cb(err, token);
                    });
            }
        ], function(err, token) {
            if (!token) {
                res.status(403)
                    .json({
                        err: {
                            message: (err || {})
                                .message || 'Token Expired'
                        }
                    });
            } else {
                logger.silly('responding with 200');
                res.status(200)
                    .json({
                        err: err,
                        data: {
                            email: token.user.email,
                            firstName: token.user.firstName,
                            lastName: token.user.lastName,
                            _id: token.user._id
                        }
                    });
            };

        });
    }
};
