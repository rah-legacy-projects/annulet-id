var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-auth-models')
    .modeler,
    crypto = require('crypto'),
    moment = require('moment'),
    configuration = require('annulet-config'),
    Mandrill = require('mandrill-api')
    .Mandrill,
    util = require('util');

require('annulet-config');
module.exports = exports = {
    validateSlug: function(req, res, next) {
        logger.silly('validating slug');
        modeler.db({}, function(err, models) {
            models.Activation.findOne({
                slug: req.params.slug,
                used: false,
                $or: [{
                    expirationDate: null
                }, {
                    expirationDate: {
                        $gte: new Date()
                    }
                }]
            })
                .exec(function(err, activation) {
                    logger.silly('activation: ' + util.inspect(activation));
                    if (!activation) {
                        res.err = 'Password reset not found.';
                    } else {
                        res.err = err;
                        res.data = activation;
                    }
                    next();
                });
        });
    },
    cancel: function(req, res, next) {
        async.waterfall([

            function(cb) {
                //get models
                modeler.db({}, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                //identify user
                models.Activation.findOne({
                    slug: req.params.slug
                })
                    .exec(function(err, activation) {
                        activation.used = true;
                        activation.save(function(err) {
                            res.err = err;
                            cb(err, models, p);
                        });
                    });
            },
        ], function(err, models, p) {
            res.err = err;
            next();
        });
    },
    changePassword: function(req, res, next) {
        async.waterfall([

            function(cb) {
                //get models
                modeler.db({}, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                logger.silly('[change pw] identifying slug');
                //identify user
                models.Activation.findOne({
                    slug: req.params.slug
                })
                    .exec(function(err, activation) {
                        p.activation = activation;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                logger.silly('[change pw] identifying user');
                models.User.findOne({
                    _id: p.activation.user
                })
                    .exec(function(err, user) {
                        p.user = user;
                        cb(err, models, p);
                    });
            },
            function(models, p, cb) {
                logger.silly('[change pw] spending slug');
                //spend the slug
                p.activation.used = true;
                p.activation.save(function(err) {
                    if(!!err){
                        logger.error('[change pw] error spending slug: ' + util.inspect(err));
                    }
                    cb(err, models, p);
                });
            },
            function(models, p, cb) {
                logger.silly('[change pw] updating pword');
                //save the updated password
                p.user.password = req.body.password;
                p.user.save(function(err) {
                    if(!!err){
                        logger.error('[change pw] error updating pword: ' + util.inspect(err));
                    }
                    cb(err, models, p);
                });
            }
        ], function(err, models, p) {
            res.err = err;
            next();
        });
    },
    activation: function(req, res, next) {
        //add slugged record
        async.waterfall([

            function(cb) {
                //get models
                modeler.db({}, function(err, models) {
                    cb(err, models, {});
                });
            },
            function(models, p, cb) {
                logger.silly('[activation] looking for email: ' + req.body.email);
                //identify user
                models.User.findOne({
                    email: req.body.email
                })
                    .exec(function(err, user) {
                        if (!user) {
                            cb({problem: 'Email address not found.'});
                        } else {
                            p.user = user;
                            cb(err, models, p);
                        }
                    });
            },
            function(models, p, cb) {
                logger.silly('[activation] adding reset slug');
                //add activation password
                var slug = (function(len) {
                    var buf = [];
                    chars = 'ABCDEGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                    charlen = chars.length;
                    for (var i = 0; i < len; ++i) {
                        buf.push(chars[Math.floor(Math.random() * (charlen + 1))])
                    }
                    return buf.join('');
                })(32);
                (new models.Activation({
                    user: p.user._id,
                    slug: slug,
                    expirationDate: moment()
                        .add(3, 'days')
                        .toDate(),
                    used: false
                }))
                    .save(function(err, activation) {
                        if(!!err){
                            logger.error('[activation] ' + util.inspect(err));
                        }
                        p.activation = activation;
                        cb(err, models, p);
                    });
            },
        ], function(err, models, p) {
            if(!!err && !!err.problem){
                res.data = err;
            }else{
                res.err = err;
                res.data = p.activation;
            }
            next();
        });
    }
};
