var express = require('express'),
    logger = require('winston'),
    loggedIn = require('annulet-config')
    .middleware.loggedIn,
    responseHandler = require('annulet-config')
    .middleware.responseHandler,
    controllerContext = require('../controllers');

var activationRouter = express.Router();
activationRouter.get('/validateSlug/:slug', controllerContext.activation.validateSlug, responseHandler);
activationRouter.post('/', controllerContext.activation.activation, responseHandler);
activationRouter.post('/cancel/:slug', controllerContext.activation.cancel, responseHandler);
activationRouter.post('/change/:slug', controllerContext.activation.changePassword, responseHandler);
module.exports = exports = activationRouter;
