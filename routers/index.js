module.exports = exports = {
	activation: require("./activation"),
	auth: require("./auth"),
	forgotPassword: require("./forgotPassword"),
	health: require("./health"),
	user: require("./user"),
};
