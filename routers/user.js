var express = require('express'),
    logger = require('winston'),
    internal = require('annulet-config').middleware.internal,
    responseHandler = require('annulet-config').middleware.responseHandler,
    controllerContext = require('../controllers');

var authRouter = express.Router();
authRouter.get('/get/:user', internal, controllerContext.user.getUser, responseHandler);
authRouter.post('/save/:user', internal, controllerContext.user.saveUser, responseHandler);
module.exports = exports = authRouter;
