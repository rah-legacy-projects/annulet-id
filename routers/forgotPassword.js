var express = require('express'),
    logger = require('winston'),
    loggedIn = require('annulet-config')
    .middleware.loggedIn,
    responseHandler = require('annulet-config')
    .middleware.responseHandler,
    controllerContext = require('../controllers');

var forgotPasswordRouter = express.Router();
forgotPasswordRouter.get('/validateSlug/:slug', controllerContext.forgotPassword.validateSlug, responseHandler);
forgotPasswordRouter.post('/forgot', controllerContext.forgotPassword.forgot, responseHandler);
forgotPasswordRouter.post('/cancel/:slug', controllerContext.forgotPassword.cancel, responseHandler);
forgotPasswordRouter.post('/change/:slug', controllerContext.forgotPassword.changePassword, responseHandler);
module.exports = exports = forgotPasswordRouter;
