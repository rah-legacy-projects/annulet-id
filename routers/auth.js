var express = require('express'),
    logger = require('winston'),
    loggedIn = require('annulet-config').middleware.loggedIn,
    controllerContext = require('../controllers');

var authRouter = express.Router();
authRouter.get('/getUserInfo', controllerContext.auth.getUserInfo);
authRouter.post('/login', controllerContext.auth.login);
authRouter.post('/logout', loggedIn, controllerContext.auth.logout);
module.exports = exports = authRouter;
