/**
 * Module dependencies.
 */
var express = require("express"),
    util = require("util"),
    http = require("http"),
    request = require('request'),
    path = require("path"),
    fs = require('fs'),
    async = require('async'),
    config = require('annulet-config'),
    _ = require('lodash'),
    mo = require('method-override'),
    cors = require('cors'),
    modeler = require('annulet-auth-models').modeler,
    useragent = require('express-useragent'),
    routers = require('./routers');

var logger = require('winston');

process.on('uncaughtException', config.middleware.uncaughtException);

var app = express();

logger.info('starting application...');
app.set("port", process.env.PORT || config.configuration.ports.authPort());
app.use(require('body-parser')());
app.use(mo('X-HTTP-Method-Override'));
app.use(cors());
app.use(require('errorhandler')());

app.use(useragent.express());
app.use(function(req, res, next) {
    logger.log('info', '', {
        method: req.method,
        url: req.originalUrl
    });
    next();
});

app.use('/auth', routers.auth);
app.use('/auth/user', routers.user);
app.use('/auth/health', routers.health);
app.use('/status', routers.health);
app.use('/auth/forgotPassword', routers.forgotPassword);
app.use('/auth/activation', routers.activation);
modeler.setup({authDb: config.configuration.paths.authDb()}, function(err) {
    logger.silly('modeler setup complete');
    var server = http.createServer(app)
        .listen(app.get("port"), function() {
            logger.info("Express server listening on port " + app.get("port"));
        });
});
